ExUnit.start

Mix.Task.run "ecto.create", ~w(-r SimpleAppReactElixir.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r SimpleAppReactElixir.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(SimpleAppReactElixir.Repo)

