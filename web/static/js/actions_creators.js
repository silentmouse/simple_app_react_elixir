export function setState(state) {
    return {
        type: 'SET_STATE',
        state
    };
}

export function addUser(entry) {
    return {
        type: 'ADD_USER',
        entry
    };
}

export function deleteUser(entry) {
    return {
        type: 'DELETE_USER',
        entry
    };
}

// export function next() {
//     return {
//         meta: {remote: true},
//         type: 'NEXT'
//     };
// }