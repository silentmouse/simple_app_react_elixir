import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, hashHistory} from 'react-router';
import {List,Map} from "immutable";
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import reducer from './reducer';
import App from "./components/App";
import {UsersContainer} from "./components/Users"

// const users = List.of({id: 1, name: "maxim",email: "email1@email.ru"});



// const addUser = () =>{
//   this.users.set(0,{id: 3, name: "sasha",email: "email3@email.ru"});
//   console.log(users);
//   console.log("bla");
// }

const store = createStore(reducer);


store.dispatch({
   type: "SET_STATE",
   state:{
       users:
           [
               {
                   id: 1,
                   name: "maxim",
                   email: "email1@email.ru"
               },
               {
                   id: 2,
                   name: "maxim2",
                   email: "email3@email.ru"
               }
           ]
   }
});



const routes = <Route component={App}>
    <Route path="/" component={UsersContainer} />
</Route>;


ReactDOM.render(
    <Provider store={store}>
        <Router history={hashHistory}>{routes}</Router>
    </Provider>,
    document.getElementById('app')
);