import React from 'react';

export default React.createClass({
    // user: function() {
    //     return this.props.get("user") || {};
    // },
    render: function() {
        return <div className="user" key={this.props.user.get("id")}>
                 <div className="inline-block user-name user-column-val">{this.props.user.get("name")}</div>
                 <div className="inline-block user-email user-column-val">{this.props.user.get("email")}</div>
                 <div className="inline-block user-actions user-column-val">
                     <a href="#" onClick={() => this.props.action_delete(this.props.user) }>Удалить</a>
                 </div>

               </div>
    }
});