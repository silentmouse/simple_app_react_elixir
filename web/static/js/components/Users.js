import React from 'react';
import User from './User';
import {Map} from "immutable";
import {connect} from "react-redux";
import * as ActionCreators from "../actions_creators";


// function addUsers(state){
//     this.props.users.set(0,{id: 3, name: "sasha",email: "email3@email.ru"});
// }


export const Users = React.createClass({
    getUsers: function() {
        return this.props.users || [];
    },
    newUserName: function() {
        var name = $(".user_name_class").val();
        var email = $(".user_email_class").val();
        this.props.addUser(Map({id: 3, name: name, email: email}));
        return 0

        // return name;
        // return this.props.new_user_name ;
    },
    render: function() {
        return <div className="users">
            <input className="user_name_class" name="user_name"></input>
            <input className="user_email_class" name="user_email"></input>
            <button onClick={() => this.newUserName()}>AddUser</button>
            {this.getUsers().map(el =>
                <User key={el} user={el} action_delete={this.props.deleteUser}/>
              )
            }
        </div>;
    }
});


function mapStateToProps(state) {
    return {
        users: state.get('users'),
        new_user_name: " "
    };
}

// export default connect(mapStateToProps)(Users);
// export default connect(mapStateToProps)(Users);
export const UsersContainer = connect(mapStateToProps,ActionCreators)(Users);