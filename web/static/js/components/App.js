import React from 'react';

// const App = () => (
//    render: function(){
//     return this.props.children
//    }
// )

export default React.createClass({
    render: function () {
        return this.props.children;
    }
});
