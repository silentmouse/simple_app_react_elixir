import {List,Map} from 'immutable';

function setState(state, newState) {
    return state.merge(newState);
}

function addUser(state, entry) {
    const users = state.get("users");
    const user_new = users.push(entry);
    return state.set("users", user_new);
}

function deleteUser(state, entry) {
    const users = state.get("users");
    const index_o = users.indexOf(entry);
    const user_new = users.remove(index_o);
    return state.set("users", user_new);
}

export default function(state = Map(), action) {
    switch (action.type) {
        case 'SET_STATE':
            return setState(state, action.state);
        case 'ADD_USER':
            return addUser(state, action.entry);
        case 'DELETE_USER':
            return deleteUser(state, action.entry);
    }
    return state;
}
