defmodule SimpleAppReactElixir.PageController do
  use SimpleAppReactElixir.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
