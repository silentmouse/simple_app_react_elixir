defmodule SimpleAppReactElixir.UsersController do
  use SimpleAppReactElixir.Web, :controller

  alias SimpleAppReactElixir.{Repo, User}

#  plug :action

  plug :scrub_params, "user" when action in [:create,:delete]

  def index(conn, _params) do
    users = Repo.all(User)
    render conn, users: users
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.changeset(%User{}, user_params)
    case Repo.insert(changeset) do
          {:ok, user} ->

            conn
            |> put_status(:created)
            |> render(SimpleAppReactElixir.UsersView, "show.json", user: user)

          {:error, changeset} ->
            conn
            |> put_status(:unprocessable_entity)
            |> render(SimpleAppReactElixir.UsersView, "error.json", changeset: changeset)
        end
  end

  def delete(conn, %{"user" => user_params}) do
      user = Repo.get!(User, user_params["id"])
      case Repo.delete(user) do
            {:ok, user} ->

              conn
              |> put_status(:created)
              |> render(SimpleAppReactElixir.UsersView, "show.json", user: user)

            {:error, changeset} ->
              conn
              |> put_status(:unprocessable_entity)
              |> render(SimpleAppReactElixir.UsersView, "error.json", changeset: changeset)
      end
  end

end
