defmodule SimpleAppReactElixir.Router do
  use SimpleAppReactElixir.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", SimpleAppReactElixir do
    pipe_through :api # Use the default browser stack

    get "/users", UsersController, :index
    post "/users", UsersController, :create
    delete "/users", UsersController, :delete

  end

  scope "/", SimpleAppReactElixir do
    pipe_through :browser # Use the default browser stack

    get "*path", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", SimpleAppReactElixir do
  #   pipe_through :api
  # end
end
