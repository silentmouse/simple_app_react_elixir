defmodule SimpleAppReactElixir.UsersView do
  use SimpleAppReactElixir.Web, :view

  def render("index.json", %{users: users}) do
      users
  end

  def render("show.json", %{user: user}) do
      user
  end

  def render("error.json", %{changeset: changeset}) do
      changeset
  end

end
